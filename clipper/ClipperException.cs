namespace ClipperLib
{
	class ClipperException : System.Exception
	{
		public ClipperException(string description) : base(description) { }
	}
}