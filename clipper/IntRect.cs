#if use_int32
using cInt = System.Int32;
#else
using cInt = System.Int64;
#endif
namespace ClipperLib
{
	public struct IntRect
	{
		public cInt left;
		public cInt top;
		public cInt right;
		public cInt bottom;

		public IntRect(cInt l, cInt t, cInt r, cInt b)
		{
			this.left = l; this.top = t;
			this.right = r; this.bottom = b;
		}
		public IntRect(IntRect ir)
		{
			this.left = ir.left; this.top = ir.top;
			this.right = ir.right; this.bottom = ir.bottom;
		}
	}
}