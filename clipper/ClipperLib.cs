/*******************************************************************************
*                                                                              *
* Author    :  Angus Johnson                                                   *
* Version   :  6.4.2                                                           *
* Date      :  27 February 2017                                                *
* Website   :  http://www.angusj.com                                           *
* Copyright :  Angus Johnson 2010-2017                                         *
*                                                                              *
* License:                                                                     *
* Use, modification & distribution is subject to Boost Software License Ver 1. *
* http://www.boost.org/LICENSE_1_0.txt                                         *
*                                                                              *
* Attributions:                                                                *
* The code in this library is an extension of Bala Vatti's clipping algorithm: *
* "A generic solution to polygon clipping"                                     *
* Communications of the ACM, Vol 35, Issue 7 (July 1992) pp 56-63.             *
* http://portal.acm.org/citation.cfm?id=129906                                 *
*                                                                              *
* Computer graphics and geometric modeling: implementation and algorithms      *
* By Max K. Agoston                                                            *
* Springer; 1 edition (January 4, 2005)                                        *
* http://books.google.com/books?q=vatti+clipping+agoston                       *
*                                                                              *
* See also:                                                                    *
* "Polygon Offsetting by Computing Winding Numbers"                            *
* Paper no. DETC2005-85513 pp. 565-575                                         *
* ASME 2005 International Design Engineering Technical Conferences             *
* and Computers and Information in Engineering Conference (IDETC/CIE2005)      *
* September 24-28, 2005 , Long Beach, California, USA                          *
* http://www.me.berkeley.edu/~mcmains/pubs/DAC05OffsetPolygon.pdf              *
*                                                                              *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* This is a translation of the Delphi Clipper library and the naming style     *
* used has retained a Delphi flavour.                                          *
*                                                                              *
*******************************************************************************/

//use_int32: When enabled 32bit ints are used instead of 64bit ints. This
//improve performance but coordinate values are limited to the range +/- 46340
//#define use_int32

//use_xyz: adds a Z member to IntPoint. Adds a minor cost to performance.
//#define use_xyz

//use_lines: Enables open path clipping. Adds a very minor cost to performance.
#define use_lines

using System;
using System.Collections.Generic;

#if use_int32
using cInt = System.Int32;
#else
using cInt = System.Int64;
#endif

namespace ClipperLib
{
	using Path = List<IntPoint>;
	using Paths = List<List<IntPoint>>;

	//------------------------------------------------------------------------------
	// PolyTree & PolyNode classes
	//------------------------------------------------------------------------------

	internal class TEdge
	{
		internal IntPoint Bot;
		internal IntPoint Curr; //current (updated for every new scanbeam)
		internal IntPoint Top;
		internal IntPoint Delta;
		internal double Dx;
		internal PolyType PolyTyp;
		internal EdgeSide Side; //side only refers to current side of solution poly
		internal int WindDelta; //1 or -1 depending on winding direction
		internal int WindCnt;
		internal int WindCnt2; //winding count of the opposite polytype
		internal int OutIdx;
		internal TEdge Next;
		internal TEdge Prev;
		internal TEdge NextInLML;
		internal TEdge NextInAEL;
		internal TEdge PrevInAEL;
		internal TEdge NextInSEL;
		internal TEdge PrevInSEL;
	};

	public class IntersectNode
	{
		internal TEdge Edge1;
		internal TEdge Edge2;
		internal IntPoint Pt;
	};

	public class MyIntersectNodeSort : IComparer<IntersectNode>
	{
		public int Compare(IntersectNode node1, IntersectNode node2)
		{
			cInt i = node2.Pt.Y - node1.Pt.Y;
			if (i > 0) return 1;
			else if (i < 0) return -1;
			else return 0;
		}
	}

	internal class LocalMinima
	{
		internal cInt Y;
		internal TEdge LeftBound;
		internal TEdge RightBound;
		internal LocalMinima Next;
	};

	internal class Scanbeam
	{
		internal cInt Y;
		internal Scanbeam Next;
	};

	internal class Maxima
	{
		internal cInt X;
		internal Maxima Next;
		internal Maxima Prev;
	};

	//OutRec: contains a path in the clipping solution. Edges in the AEL will
	//carry a pointer to an OutRec when they are part of the clipping solution.
	internal class OutRec
	{
		internal int Idx;
		internal bool IsHole;
		internal bool IsOpen;
		internal OutRec FirstLeft; //see comments in clipper.pas
		internal OutPt Pts;
		internal OutPt BottomPt;
		internal PolyNode PolyNode;
	};

	internal class OutPt
	{
		internal int Idx;
		internal IntPoint Pt;
		internal OutPt Next;
		internal OutPt Prev;
	};

	internal class Join
	{
		internal OutPt OutPt1;
		internal OutPt OutPt2;
		internal IntPoint OffPt;
	};

	//------------------------------------------------------------------------------

}
