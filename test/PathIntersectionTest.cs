using System;
using Xunit;
using FluentAssertions;
using ClipperLib;
using System.Collections.Generic;

namespace test
{
	using Paths = List<List<IntPoint>>;
	using Path = List<IntPoint>;
    public class PathIntersectionTest
    {
		[Fact]
		public void TwoTrianglesIntersect_GetIntersection()
		{
			var c = new Clipper();
			var left = new Path
			{
				new IntPoint(-2, -2),
				new IntPoint(-2, 2),
				new IntPoint(1, 0)
			};
			var right = new Path
			{
				new IntPoint(2, 2),
				new IntPoint(2, -2),
				new IntPoint(-1, 0)
			};
			var leftShape = new Paths { left };
			var rightShape = new Paths { right };
			var solution = new Paths();
			c.AddPath(left, PolyType.ptSubject, true);
			c.AddPath(right, PolyType.ptClip, true);
			c.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

			solution.Should().NotBeEmpty();
		}
    }
}
