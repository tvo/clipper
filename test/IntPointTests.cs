using System;
using Xunit;
using FluentAssertions;
using ClipperLib;

namespace test
{
    public class IntPointTests
    {
        [Fact]
        public void CtorTests()
        {
			var point = new IntPoint(3, 4);
			point.X.Should().Be(3);
			point.Y.Should().Be(4);
        }

        [Fact]
        public void CtorFloatPointsTests()
        {
			var point = new IntPoint(3.555, 4.555);
			point.X.Should().Be(3);
			point.Y.Should().Be(4);
        }

        [Fact]
        public void EqualsTest_True()
        {
			var one = new IntPoint(3, 4);
			var two = new IntPoint(3, 4);
			var result = one == two;
			result.Should().BeTrue();
        }

        [Fact]
        public void EqualsTest_Fails()
        {
			var one = new IntPoint(3, 4);
			var two = new IntPoint(3, 5);
			var result = one == two;
			result.Should().BeFalse();
        }
    }
}
